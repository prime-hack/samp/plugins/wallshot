import core.sys.windows.windows;
import core.sys.windows.dll;
import llmo.mem;

extern (Windows) BOOL DllMain(HINSTANCE hinst,uint event,void*){
	final switch(event){
		case DLL_PROCESS_ATTACH:
			dll_process_attach(hinst);
			init;
			break;
		case DLL_PROCESS_DETACH:
			uninit;
			dll_process_detach(hinst);
			break;
		case DLL_THREAD_ATTACH:
			dll_thread_attach();
			break;
		case DLL_THREAD_DETACH:
			dll_thread_detach();
			break;
	}
	return TRUE;
}

void init(){
	writeMemory(0x00740701, false, MemorySafe.safe);
	writeMemory(0x00740703, false, MemorySafe.safe);
	writeMemory(0x00740709, false, MemorySafe.safe);

	writeMemory(0x00740B49, false, MemorySafe.safe);
	writeMemory(0x00740B4B, false, MemorySafe.safe);
	writeMemory(0x00740B51, false, MemorySafe.safe);

	writeMemory(0x0073620D, false, MemorySafe.safe);
	writeMemory(0x0073620F, false, MemorySafe.safe);
	writeMemory(0x00736215, false, MemorySafe.safe);
}

void uninit(){
	writeMemory(0x00740701, true, MemorySafe.safe);
	writeMemory(0x00740703, true, MemorySafe.safe);
	writeMemory(0x00740709, true, MemorySafe.safe);

	writeMemory(0x00740B49, true, MemorySafe.safe);
	writeMemory(0x00740B4B, true, MemorySafe.safe);
	writeMemory(0x00740B51, true, MemorySafe.safe);

	writeMemory(0x0073620D, true, MemorySafe.safe);
	writeMemory(0x0073620F, true, MemorySafe.safe);
	writeMemory(0x00736215, true, MemorySafe.safe);
}
